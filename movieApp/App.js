import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View
} from 'react-native';
//import Home from './pages/Home';
import MainNavigation from './MainNavigation';
import {
  Colors
} from 'react-native/Libraries/NewAppScreen';

const width = Dimensions.get('window').width; 
const height = Dimensions.get('window').height; 

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [splash, setSplash] = useState(true);
  useEffect (() => {
    setTimeout (() => {
      setSplash(false);
    }, 5000);
  },[]);


  return splash ? (
    <View style={style.container}>
        <Image style={style.image} source={require('./assets/logo.png')}/>
    </View>) : (
        <MainNavigation/>
    );
}

style = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    alignSelf: "center"
  }
})

export default App;
