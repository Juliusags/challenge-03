import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import Home from "./pages/Home";
import DetailHome from "./pages/DetailHome";

const navigate = createNativeStackNavigator();

const MainNavigation = () => {
    return (
        <NavigationContainer>
            <navigate.Navigator initialRouteName="Home">
            <navigate.Screen name="Home" component={Home} />
            <navigate.Screen name="DetailHome" component={DetailHome} />
            </navigate.Navigator>
        </NavigationContainer>
    );
}

export default MainNavigation;