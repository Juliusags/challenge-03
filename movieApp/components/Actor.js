import React, {useState, useEffect} from "react";
import axios from "axios";
import {View, Image, StyleSheet, Text} from "react-native";
import DataDetail from "./DataDetail";
import Data from "./DataDetail";

const Actor = (id) => {
    const [movie, setMovie] = useState(null);

    useEffect(() => {
        Data(id).then(res => {
            setMovie(res);
        })
    }, []);

    return (
        <View>
        {movie && (
            <View style={style.card}>
                <Image source={{uri: movie.profile_path}}/>
                <Text source={{uri: movie.name}}/>
            </View>
        )}
        </View>
        );

}

style = StyleSheet .create({
    card : {
        flex : 1,
        alignItems: "center",
        justifyContent: "center",
        width: 56,
        height: 77
    }
})



export default Actor;