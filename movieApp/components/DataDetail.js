import React from "react";
import axios from "axios";


async function Data(id){
    try {
        const response = await axios.get("http://code.aldipee.com/api/v1/movies/" + id);
        return response.data;
      } catch (error) {
        console.error(error);
      }
}

export default Data;