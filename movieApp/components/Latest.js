import React, {useEffect, useState} from "react";
import {RefreshControl, Button, Dimensions, ScrollView, Image, StyleSheet, View, Text} from "react-native";
import Data from "./Data";
import DetailHome from "../pages/DetailHome";
import { useNavigationState } from "@react-navigation/native";

const width = Dimensions.get('window').width;
const wait = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
} 


const Latest = ({navigation}) => {
    const [movie, setMovie] = useState([]);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        Data().then(res => {
            setMovie(res);
        })
    }, []);

    const onRefresh = React.useCallback(() => {
        wait(2000).then(() => setMovie(movie));
    }, [movie]);

    return(
    <View>
        <Text style={style.titleText}>Latest Upload</Text>
        <ScrollView refreshControl={
            <RefreshControl refreshing={refreshing}
             onRefresh={onRefresh} />
        }>
            {movie.map((item, index) => {
                return(
            <View style={style.card} key={index}>
                <View style={style.content}>
                    <Image style={style.image} source={{uri: item.poster_path}}/>
                    <View style={style.text}>
                        <Text style={{fontSize: 20, fontFamily: "Open Sans", fontWeight: "bold"}}>{item.original_title}</Text>
                        <Text style={{fontSize: 12}}>{item.release_date}</Text>
                        <Text>{item.vote_average}</Text>
                        <Button title="Show More" onPress={() => {navigation.navigate('DetailHome',{id:item.id})}}/>
                    </View>
                </View>
            </View>
                )
            })}
        </ScrollView>
    </View>
    )
}

style = StyleSheet.create({
    card:{
        flex: 1,
        flexDirection: "row",
        alignSelf: "flex-start",
        width: width,
        height: 167,
        marginTop: 10
    },
    content:{
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-start",
        
    },
    text:{
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-start",
        
    },
    image:{
        width: 117,
        height: 167,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        borderRadius: 10,
        marginRight: 20
    },
    titleText:{
        paddingTop: 10,
        color: "black",
        fontSize: 15,
        fontWeight: 'bold',
    }
})

export default Latest;