import React, {useEffect, useState} from "react";
import {RefreshControl, ScrollView, Image, StyleSheet, View, Text} from "react-native";
import Data from "./Data.js";

const wait = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
} 

const Recommended = () => {
    const [movie, setMovie] = useState([]);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        Data().then(res => {
            setMovie(res);
        })
    }, []);

    const onRefresh = React.useCallback(() => {
        wait(2000).then(() => setMovie(movie));
    }, [movie]);

    return(
    <View>
        <Text style={style.titleText}>Recommended</Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} 
            refreshControl={
            <RefreshControl refreshing={refreshing}
             onRefresh={onRefresh} />
        }>
            {movie.map((item, index) => {
                return(
                    <View key={index}>
                        <Image style={style.image} source={{uri: item.poster_path}}/>
                    </View>
                )})}
        </ScrollView>
    </View>
    )
}

style = StyleSheet.create({
    image : {
        borderRadius: 10,
        width: 147,
        height: 210,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10

    },
    titleText:{
        color: "black",
        fontSize: 15,
        fontWeight: 'bold',
    }
})

export default Recommended;