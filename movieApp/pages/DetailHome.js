import { useNavigationState } from "@react-navigation/native";
import React, {useState, useEffect} from "react";
import {RefreshControl, StyleSheet, Dimensions, ScrollView, View, Image, Text} from "react-native";
import Actor from "../components/Actor";
import Data from "../components/DataDetail";

const width = Dimensions.get('window').width; 
const height = Dimensions.get('window').height;
const wait = (timeout) => { 
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
} 

const DetailHome = ({navigation}) => {
    const id = useNavigationState(state => state.routes[state.index].params.id);
    const [movie, setMovie] = useState(null);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        Data(id).then(res => {
            setMovie(res);
        })
    }, []);

    const onRefresh = React.useCallback(() => {
        wait(2000).then(() => setMovie(movie));
    }, [movie]);

    return (
       <View style={style.container}>
            <ScrollView showsHorizontalScrollIndicator={false} 
            refreshControl={
            <RefreshControl refreshing={refreshing}
             onRefresh={onRefresh} />
        }>
            {movie && (
            <View>
                <Image style={style.bgImage} source={{uri: movie.backdrop_path}}/>
                    <View style={style.card}>
                        <Image source={{uri: movie.poster_path}}/>
                        <View style={style.text}>
                            <Text style={{color:"black",fontSize: 24, fontFamily: "Open Sans", fontWeight: "bold"}}>{movie.original_title}</Text>
                            <Text style={{fontSize: 12, color:"black"}}>{movie.release_date}</Text>
                            <Text style={{color:"black"}}>{movie.vote_average}</Text>
                        </View>
                    </View>
                    <View style={style.content}>
                        <Text style={style.titleText}>Genres</Text>
                        <View style={{justifyContent:"space-evenly", flexDirection:"row"}}>
                        {movie.genres.map((item, index) => {
                            return(
                                <Text key={index}>{item.name}</Text>
                            )
                        })}  
                        </View>
                    </View>
                    <View style={style.content}>
                        <Text style={style.titleText}>Synopsis</Text>
                        <Text>{movie.overview}</Text>     
                    </View>
                    <View style={style.content}>
                        <Text style={style.titleText}>Actors</Text>
                        <Actor/>
                    </View>
            </View>
            )}     
            </ScrollView>
        </View>
    );

}

style = StyleSheet.create({
    container:{
        flex: 1,
        width: width,
        height: height
    },
    content:{
        flex: 1,
        paddingBottom: 30,
        
        
    },
    titleText:{
        color: "black",
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 5

    },
    card:{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        height: 189,
        marginTop: 66
    },
    text:{
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-start",
    },
    bgImage : {
        flex: 1,
        width: width,
        height: "20%",
    } 
});

export default DetailHome;