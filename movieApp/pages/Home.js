import React from "react";
import {Alert, Image, ScrollView, Dimensions, Text, View, StyleSheet} from "react-native";
import NetInfo from "@react-native-community/netinfo";
import Recommended from "../components/Recommended";
import Latest from "../components/Latest";

const width = Dimensions.get('window').width; 
const height = Dimensions.get('window').height;

const Home = ({navigation}) => {

    NetInfo.fetch().then(state => {
        console.log("Connection type", state.type);
        console.log("Is connected?", state.isConnected);
      });

    return (
        <View style={style.container}>
            <View style={style.header}>
                <Recommended/>
            </View>
            <View style={style.main}>
                <Latest navigation={navigation}/>
            </View>
        </View>
    );
}

style = StyleSheet.create({
    container: {
        flex: 1,
        width: width,
        height: height,
        marginLeft: 20,
        marginTop: 20,
        marginRight: 20
    },
    header:{
        flex: 2,
    },
    main:{
        flex: 3
    }
});

export default Home;